<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            'name' => 'testgame',
            'state' => 'Created',
            'method_id' => 1
        ]);
        DB::table('games')->insert([
            'name' => 'activeGame',
            'state' => 'Active',
            'method_id' => 1
        ]);
        DB::table('games')->insert([
            'name' => 'finishedGame',
            'state' => 'Finished',
            'method_id' => 1
        ]);
        DB::table('games')->insert([
            'name' => 'testgame2',
            'state' => 'Created',
            'method_id' => 1
        ]);
        DB::table('games')->insert([
            'name' => 'activeGame2',
            'state' => 'Active',
            'method_id' => 1
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessageReceiverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('message_user')->insert([
            'message_id' => 1,
            'user_id' => 6
        ]);

        DB::table('message_user')->insert([
            'message_id' => 2,
            'user_id' => 1
        ]);
    }
}

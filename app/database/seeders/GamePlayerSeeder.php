<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GamePlayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('game_player')->insert([
            'game_id' => 1,
            'player_id' => 1,
        ]);

        DB::table('game_player')->insert([
            'game_id' => 1,
            'player_id' => 2,
        ]);

        DB::table('game_player')->insert([
            'game_id' => 1,
            'player_id' => 3,
        ]);

        DB::table('game_player')->insert([
            'game_id' => 2,
            'player_id' => 1,
        ]);

        DB::table('game_player')->insert([
            'game_id' => 3,
            'player_id' => 1,
        ]);
    }
}

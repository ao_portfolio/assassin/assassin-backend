<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameSupervisorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('game_supervisor')->insert([
            'game_id' => 1,
            'user_id' => 6,
        ]);

        DB::table('game_supervisor')->insert([
            'game_id' => 1,
            'user_id' => 7,
        ]);
    }
}

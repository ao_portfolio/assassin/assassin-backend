<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->insert([
            'sender_id' => 1,
            'game_id' => 1,
            'content' => 'Somewhere over the rainbow, way up high, where big birds fly.'
        ]);

        DB::table('messages')->insert([
            'sender_id' => 6,
            'game_id' => 1,
            'content' => 'Don\'t stop me now'
        ]);
    }
}

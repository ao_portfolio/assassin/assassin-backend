<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'bart delrue',
            'email' => 'bart.delrue@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'player_id' => 1,
            'role' => 'player'
        ]);

        DB::table('users')->insert([
            'name' => 'joris maervoet',
            'email' => 'joris.maervoet@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'player_id' => 2,
            'role' => 'player'
        ]);

        DB::table('users')->insert([
            'name' => 'player test1',
            'email' => 'test1@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'player_id' => 3,
            'role' => 'player'
        ]);

        DB::table('users')->insert([
            'name' => 'player test2',
            'email' => 'test2@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'player_id' => 4,
            'role' => 'player'
        ]);

        DB::table('users')->insert([
            'name' => 'player test3',
            'email' => 'test3@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'player_id' => 5,
            'role' => 'player'
        ]);

        DB::table('users')->insert([
            'name' => 'supervisor1',
            'email' => 'supervisor1@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'role' => 'supervisor'
        ]);

        DB::table('users')->insert([
            'name' => 'supervisor2',
            'email' => 'supervisor2@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'role' => 'supervisor'
        ]);

        DB::table('users')->insert([
            'name' => 'supervisor3',
            'email' => 'supervisor3@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'role' => 'supervisor'
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@odisee.be',
            'password' => Hash::make('Azerty123'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'role' => 'admin'
        ]);
    }
}

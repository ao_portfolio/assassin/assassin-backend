<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;

class APIMessageController extends Controller
{
    public function viewMessages(Request $request){
        if(Auth::user()->role ===  'player'){
            $messages = Message::where('game_id', $request->id)->where(function($q) {
                $q->whereHas('receivers', function($q){
                    $q->where('id', Auth::user()->id);
                })->orWhere('sender_id', Auth::user()->id);
            })->with(["sender:id,name", "receivers:id,name"])->orderBy('created_at', 'asc')->get();
            // $receivedMessages = Message::where('game_id', $request->game_id)->where('receiver_id', Auth::user()->id)->orderBy('created_at', 'desc');
            // $sendMessages = Message::where('game_id', $request->game_id)->where('sender_id', Auth::user()->id)->orderBy('created_at', 'desc');
        } 
        else{
            $messages = Message::where('game_id', $request->id)
                ->where('sender_id', $request->playerId)
                ->orWhereHas('receivers', function($q){
                    $q->where(['id', $request->playerId]);
                })->orderBy('created_at', 'asc')
                ->get();
        }
        
        return ['data'=> $messages];
    }

    public function viewPlayerMessages(Request $request){
        if(Auth::user()->role !==  'player'){
            $messages = Message::where('game_id', $request->id)->where(function($q) {
                    $q->whereHas('receivers', function($q){
                        $q->where('id', Auth::user()->id);
                    })->orWhere('sender_id', Auth::user()->id);
                })->with(["sender:id,name", "receivers:id,name"])
                ->orderBy('created_at', 'asc')
                ->get();
        }
        else{
            abort(401);
        }
        return ['data'=> $messages];
    }

    public function viewLatestPlayerMessages(Request $request){
        $allPlayers = Game::findOrFail($request->id)->players()->select('id','username', 'isDead')->get();
        $messages = [];
        foreach($allPlayers as $player){
            $message = Message::where('game_id', $request->id)->where(function($q) use ($player) {
                    $q->whereHas('receivers', function($q) use ($player){
                        $q->where('id', $player->id);
                    })->orWhere('sender_id', $player->id);
                })
                ->with(["sender:id,name", "receivers:id,name"])
                ->orderBy('created_at', 'desc')
                ->limit(1)
                ->get()->first();
            
            $player->message = $message;
            $messages[] = $player;
        }
        usort($messages, function($a, $b) {return strcmp($b->message->created_at ?? '000' , $a->message->created_at ?? '000');}); //players zonder messages worden achteraan in de lijst geplaatst door '000'
        return ['data'=> $messages];
    }

    public function viewArchived(Request $request){
        if(Auth::user()->role !==  'player'){
            $messages = Message::where('game_id', $request->id)->where('archived', true)->with(["sender:id,name", "receivers:id,name"])->orderBy('created_at', 'desc')->get();
        }
        else{
            abort(401);
        }
        return ['data'=> $messages];
    }

    public function send(Request $request){
        if($request->receiver === "supervisors"){
            $request->receiver = Game::findOrFail($request->id)->supervisors()->pluck('id');
        }
        else if($request->receiver === "allPlayers"){
            $request->receiver = Game::findOrFail($request->id)->players()->pluck('id');
        }

        $message = Message::create(['content'=>$request->content, 'sender_id'=>Auth::user()->id, 'game_id'=>$request->id]);
        foreach($request->receiver as $receiver_id){
           $message->receivers()->attach($receiver_id);
        }
        
        return response(201);
    }

    public function archive(Request $request){
        if(Auth::user()->role !==  'player'){
            Message::where('id', $request->messageId)->update(["archived" => true]);
        }
        else{
            abort(401);
        }
        return response(200);
    }
    public function unarchive(Request $request){
        if(Auth::user()->role !==  'player'){
            Message::where('id', $request->messageId)->update(["archived" => false]);
        }
        else{
            abort(401);
        }
        return response(200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\GamePlayer;
use App\Models\Game;
use Illuminate\Support\Facades\Auth;

class APIPlayerController extends Controller
{
    public function show()
    {
        $player = Player::findOrFail(Auth::user()->player_id)->select('username')->withCount(["games as totalGames", "activeGames as activeGames"])->first();
        $player["totalKills"] = Player::findOrFail(Auth::user()->player_id)->gamePlayers()->sum("killCount");
        return ['data'=> $player];
    }

    public function killPlayer(Request $request){
        if(Game::findOrFail($request->id)->state === "Active" && (Auth::user()->player_id === $request->playerId || Auth::user()->role !== "player")){
            $newPlayers = GamePlayer::where("game_id",$request->id)->whereNull("target_id")->select("player_id")->get()->shuffle(); //new players

            $killedPlayersTarget = GamePlayer::where('game_id', $request->id)->where('player_id', $request -> playerId)->select("target_id")->first()->target_id; //get target from dead
            $killerId = GamePlayer::where('game_id', $request->id)->where('target_id', $request -> playerId)->select("player_id")->firstOrFail()->player_id; //get killer
            $rank = Game::withCount(["players as playersAlive" => function($query){
                $query->where('isDead', false);
            }])->findOrFail($request->id)->playersAlive;
            GamePlayer::where('game_id', $request->id)->where('player_id', $request -> playerId)->update(['isDead' => true, 'rank' => $rank]); //kill player and set rank

            if(!$newPlayers->isEmpty()){
                GamePlayer::where('game_id', $request->id)->where('player_id','!=', $request -> playerId)->whereNotNull('rank')->increment('rank', $newPlayers->count());
                foreach($newPlayers as $player){
                    $target = $previous ?? $killedPlayersTarget;
                    dump($target);
                    GamePlayer::where('game_id', $request->id)->where('player_id', $player->player_id)->update(['target_id' => $target]); //set target new player
                    $previous = $player->player_id;
                }
                $killersNextTarget = $previous;
            }
            else{
                $killersNextTarget = $killedPlayersTarget;
            }
            GamePlayer::where('game_id', $request->id)->where('player_id', $killerId)->update(['target_id' => $killersNextTarget]);
            GamePlayer::where('game_id', $request->id)->where('player_id', $killerId)->increment("killCount");
            if($killerId === $killersNextTarget){
                GamePlayer::where('game_id', $request->id)->where('player_id', $killerId)->update(['rank' => 1]);
            }
        }
        else{
            abort(401);
        }
        return response("Kill sequence concluded", 204);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view("login");
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if(Auth::user()->role=="admin"){
                return redirect(url('/'));
            }
            else{
                Auth::logout();
                $request->session()->invalidate();
            }
        }
        return response(['message' => 'The provided credentials do not match our records.'], 401);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;
use App\Models\User;
use App\Models\Player;
use App\Models\Method;
use App\Models\GamePlayer;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $games = Game::with("method")->withCount(["players as playersAlive" => function($query){
            $query->where('isDead', false);
        }])
        ->when($request->search, function($query) use ($request){
            $terms = explode(" ", $request->search);
            foreach($terms as $term) {
                $query->where("name", "like", "%" . $term . "%");
            }
        })
        ->when($request->sortBy, function($query) use ($request){
            switch($request->sortBy){
                case("name"):
                    $query->orderBy('name', 'asc');
                    break;
                case("endDate"):
                    $query->orderBy('endDate', 'desc');
                    break;
                case("players"):
                    $query->orderBy('playersAlive', 'desc');
                    break;
                case("gameState"):
                    $query->orderBy('state', 'desc');
                    break;
                case("killMethod"):
                    $query->orderBy('method.name', 'desc');
                    break;
                default:
            } 
        })->get();
        return view("homepage", ["games" => $games, "request" => $request]);
    }
    public function viewDetails(Request $request)
    {
        $game = Game::with(["method", "players"])
        ->withCount(["players as playersAlive" => function($query){
            $query->where('isDead', false);
        }])
        ->findOrFail($request->id);
        $potentialPlayers = User::where("role", "player")->whereNotIn("id", $game->players->pluck('id'))->get();
        return view("details", ["game" => $game, "potentialPlayers" => $potentialPlayers]);
    }
    public function viewDashboard(Request $request)
    {
        $game = Game::findOrFail($request->id);
        $players = GamePlayer::where('game_id', $request->id)->with(["player:id,username","target:id,username"])->get();
        return view("dashboard", ["game" => $game, "players"=>$players]);
    }
    public function viewAdd(Request $request){
        $methods = Method::all();
        $supervisors = User::where("role", "supervisor")->orWhere('role', 'admin')->get();

        return view('add', ['methods' => $methods, 'supervisors' => $supervisors, 'request' => $request]);
    }
    public function add(Request $request){
        $validated = $request->validate([
            'name' => 'required|max:255',
            'method' => 'required',
            'endDate' => 'nullable|date',
        ]);
        $game = Game::create([
            'name' => $request->name,
            'method_id' => $request->method,
            'endDate' => $request->endDate,
        ]);
        foreach($request->supervisors as $supervisorId){
            $supervisor = User::where('role', 'supervisor')->orWhere('role', 'admin')->findOrFail($supervisorId);
            $game->supervisors()->attach($supervisor);
        }

        return redirect('/');
    }

    public function addPlayer(Request $request){
        if(User::select('role')->where('player_id', $request->player)->firstOrFail()->role === "player"){
            $game = Game::findOrFail($request->id)->players()->attach($request->player);
        }
        else{
            abort(400);
        }
       
        return redirect(url("/games/$request->id"));
    }

    public function removePlayer(Request $request){
        foreach($request->players as $playerId){
            $game = Game::findOrFail($request->id)->players()->detach($playerId);
        }    
        return redirect(url("/games/$request->id"));        
    }
    
    public function start(Request $request){
        $query = Game::where('id', $request->id)->where('state', 'Created')->update(['state' => 'Active']);

        if($query){
            $players = Game::findOrFail($request->id)->players()->pluck('id');
            $shuffled = $players->shuffle();
            foreach($shuffled as $player){
                $target = $previous ?? $shuffled->last();
                GamePlayer::where('game_id', $request->id)->where('player_id', $player)->update(['target_id' => $target]);
                $previous = $player;
            }
        }
        
        return redirect(url("/games/$request->id")); 
    }

    public function stop(Request $request){
        Game::where('id', $request->id)->where('state', 'Active')->update(['state' => 'Finished']);
        return redirect(url("/games/$request->id")); 
    }

    public function remove(Request $request){
        GamePlayer::where('game_id', $request->id)->delete();
        Game::where('id', $request->id)->delete();
        return redirect(url("/")); 
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Game;
use App\Models\User;
use App\Models\Player;

class APIGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->role === 'player'){
            $games = Game::select(['id', 'name', 'endDate', 'state'])->withCount(["players as playerCount"])->whereHas('players', function ($query) {
                $query->where('id', Auth::user()->player_id);
            })->with(["players" => function($query){
                $query->select("isDead")->where("player_id", Auth::user()->player_id)->get();
            }])->get();
            foreach($games as $game){
                $game["player"] = $game["players"][0];
                unset($game["players"]);
            }
        } else {
            $games = User::findOrFail(Auth::user()->id)->games()->withCount(["players as playerCount"])->get();
        }
        
        // $games["player"] = Game::findOrFail($id)->gamePlayers()->where("player_id", Auth::user()->player_id)->select("isDead")->first();
        return ['data'=> $games];
    }
    public function view(Request $request)
    {
        $data = Game::withCount(['players as playerCount'])->findOrFail($request->id);
        return ['data'=> $data];
    }
    public function viewDetails(Request $request){
        $data = Game::withCount(['players as playerCount'])
            ->with(["method"])
            ->withCount(["players as playersAlive" => function($query){
                $query->where('isDead', false);
            }])
            ->findOrFail($request->id);
        $data["player"] = Game::findOrFail($request->id)->gamePlayers()->where("player_id", Auth::user()->player_id)->select("killCount", "isDead", "target_id", "player_id")->with("target:id,username")->first();
        return ['data'=> $data];
    }

    public function viewJoinable(){
        $games = Game::select(['id', 'name', 'endDate'])->withCount(["players as playerCount"])->where("state", "Created")->whereDoesntHave('players',
            function ($query) {
                $query->where('id', Auth::user()->player_id);
            }
        )->get();
        return ['data'=> $games];
    }

    public function join(Request $request){
        Game::findOrFail($request->id)->players()->attach(Auth::user()->player_id);
        return response('Added player', 204);
    }
}

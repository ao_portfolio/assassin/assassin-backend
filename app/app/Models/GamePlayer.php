<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class GamePlayer extends Pivot
{
    use HasFactory;
    public $timestamps = false;

    public function game(){
        return $this->belongsTo(Game::class);
    }

    public function player(){
        return $this->belongsTo(Player::class);
    }

    public function target(){
        return $this->belongsTo(Player::class);
    }
}

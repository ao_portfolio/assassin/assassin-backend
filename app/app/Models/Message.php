<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $fillable = ["content","sender_id","game_id","created_at","updated_at"];

    public function receivers(){
        return $this->belongsToMany(User::class);
    }

    public function sender(){
        return $this->belongsTo(User::class);
    }
}

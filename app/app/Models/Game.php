<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Game extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'method_id', 'endDate'];

    public function supervisors(){
        return $this->belongsToMany(User::class, 'game_supervisor');
    }

    public function method(){
        return $this->belongsTo(Method::class, "method_id", "id");
    }

    public function players(){
        return $this->belongsToMany(Player::class)->using(GamePlayer::class);
    }

    public function gamePlayers(){
        return $this->hasMany(GamePlayer::class);
    }
}

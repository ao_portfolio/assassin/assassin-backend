<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Game;
use App\Models\User;
use App\Models\Player;
use App\Models\Method;
use App\Models\GamePlayer;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_getPlayer()
    {
        $user = User::findOrFail(1);
        
        $response = $this->actingAs($user)->get('/api/player');
        $response->assertStatus(200);
    }
}

<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\APIPlayerController;
use App\Http\Controllers\APIMessageController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

Route::middleware('guest')->post('api/login', function (Request $request) {
    $credentials = $request->only('email', 'password');
    if (Auth::attempt($credentials)) {
        return response(['message' => 'The user has been authenticated successfully'], 200);
    }
    return response(['message' => 'The provided credentials do not match our records.'], 401);
});
Route::middleware('guest')->post('api/logout', function (Request $request) {
    Auth::guard('web')->logout();
    $request->session()->invalidate();
    return response(['message' => 'The user has been logged out successfully'], 200);
});

Route::middleware(['auth'])->get('/', [GameController::class, 'index'])->name("home");
Route::middleware(['auth'])->get('/games/add', [GameController::class, 'viewAdd']);
Route::middleware(['auth'])->post('/games/add', [GameController::class, 'add']);

Route::middleware(['auth'])->get('/games/{id}', [GameController::class, 'viewDetails'])->where(['id' => '[0-9]+']);
Route::middleware(['auth'])->post('/games/{id}/players/add', [GameController::class, 'addPlayer'])->where(['id' => '[0-9]+']);
Route::middleware(['auth'])->post('/games/{id}/players/remove', [GameController::class, 'removePlayer'])->where(['id' => '[0-9]+']);
Route::middleware(['auth'])->get('/games/{id}/dashboard', [GameController::class, 'viewDashboard'])->where(['id' => '[0-9]+']);
Route::middleware(['auth'])->post('/games/{id}/remove', [GameController::class, 'remove'])->where(['id' => '[0-9]+']);
Route::middleware(['auth'])->post('/games/{id}/start', [GameController::class, 'start'])->where(['id' => '[0-9]+']);
Route::middleware(['auth'])->post('/games/{id}/stop', [GameController::class, 'stop'])->where(['id' => '[0-9]+']);


Route::middleware('guest')->get('/login', [LoginController::class, 'index'])->name('login');
Route::middleware('guest')->post('/login', [LoginController::class, 'login']);



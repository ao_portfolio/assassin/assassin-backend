<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\APILoginController;
use App\Http\Controllers\APIPlayerController;
use App\Http\Controllers\APIGameController;
use App\Http\Controllers\APIMessageController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    $user = $request->user();
    return $user;
});

Route::middleware('auth:sanctum')->controller(APIPlayerController::class)->group(function (){
    Route::get('/player', 'show');
    Route::get('/players/{playerId}', 'show')->where(['playerId' => '[0-9]+']);
    Route::get('/players/{playerId}/games', 'games')->where(['playerId' => '[0-9]+']);
    Route::post('/games/{id}/players/{playerId}/kill', 'killPlayer')->where(['id' => '[0-9]+', 'playerId' => '[0-9]+']);

    Route::get('/games/{id}/players', 'viewPlayers')->where(['id' => '[0-9]+']);
});

Route::middleware('auth:sanctum')->controller(APIGameController::class)->group(function (){
    Route::get('/games', 'index');
    Route::get('/games/joinable', 'viewJoinable');
    Route::get('/games/{id}', 'view')->where(['id' => '[0-9]+']);
    Route::get('/games/{id}/details', 'viewDetails')->where(['id' => '[0-9]+']);

    Route::post('/games/{id}/join', 'join')->where(['id' => '[0-9]+']);
    // Route::post('/games/{id}/players/{playerId}/add', 'addPlayer')->where(['id' => '[0-9]+', 'playerId' => '[0-9]+']);
    // Route::post('/games/{id}/players/{playerId}/remove', 'removePlayer')->where(['id' => '[0-9]+', 'playerId' => '[0-9]+']);
});

Route::middleware('auth:sanctum')->controller(APIMessageController::class)->group(function (){
    
    // Route::post('/games/{id}/players/{playerId}/kill', 'killPlayer')->where(['id' => '[0-9]+', 'playerId' => '[0-9]+']);
    Route::get('/games/{id}/messages', 'viewMessages')->where(['id' => '[0-9]+']); //add filter for archived messages
    Route::get('/games/{id}/messages/latest', 'viewLatestPlayerMessages')->where(['id' => '[0-9]+']);
    Route::get('/games/{id}/messages/archived', 'viewArchived')->where(['id' => '[0-9]+']);
    Route::post('/games/{id}/messages/send', 'send')->where(['id' => '[0-9]+']);
    Route::post('/games/{id}/archive', 'archive')->where(['id' => '[0-9]+']);
    Route::post('/games/{id}/unarchive', 'unarchive')->where(['id' => '[0-9]+']);

    Route::get('/games/{id}/players/{playerId}/messages', 'viewPlayerMessages')->where(['id' => '[0-9]+', 'playerId' => '[0-9]+']);
});
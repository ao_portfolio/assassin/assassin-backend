@extends('main')

@section('content')
<main>
  <a href="{{ url('/') }}">&lt; back</a>
  <div class="container">
    
    <h2>Add new game</h2>
    <form class="needs-validation" novalidate="" method="post" action="{{ url('games/add') }}" enctype="multipart/form-data">
      @csrf
      {{-- <input type="hidden" name="_token" value="xbleOu1XHjw4MyI2FeAg5VzncRFBmSWuSE9dJk9D">                 --}}
      <div class="row g-3">
        <div class="col-12">
          <label for="name" class="form-label">Game name</label>
          <input type="text" class="form-control @if ($errors->isNotEmpty()) @error('name') is-invalid @else is-valid @enderror @endif" id="name" name="name" value="{{ $request->name }}">
          <div class="invalid-feedback">
            @error('name') {{$message}} @enderror
          </div>
        </div>
      </div>

      <div class="row g-3">
        <div class="col-md-6">
          <label for="method" class="form-label">Killing method</label>
          <select class="form-select @if ($errors->isNotEmpty()) @error('method') is-invalid @else is-valid @enderror @endif" id="method" required="" name="method">
            <option value="">Choose...</option>
            @foreach($methods as $method)
              <option value="{{$method->id}}" @if ($request->method == $method->id) selected @endif>{{$method->name}}</option>
            @endforeach
          </select>
          <div class="invalid-feedback">
            @error('method') {{$message}} @enderror
          </div>
        </div>
      </div>

      <div class="row g-3">
        <div class="col-12">
          <label for="endDate" class="form-label">End date</label>
          <input type="date" class="form-control @if ($errors->isNotEmpty()) @error('endDate') is-invalid @else is-valid @enderror @endif" id="endDate" name="endDate" value="{{ $request->endDate }}">
          <div class="invalid-feedback">
            @error('endDate') {{$message}} @enderror
          </div>
        </div>
      </div>

      <div class="supervisors-table">
        <h3>Select supervisors</h3>
        <div class="supervisors-names">
          @foreach($supervisors as $supervisor)
          <div>
            <input class="visually-hidden" type="checkbox" value="{{$supervisor->id}}" id="supervisor{{$supervisor->id}}" name="supervisors[]"><label for="supervisor{{$supervisor->id}}">{{$supervisor->name}}</label>
          </div>
          @endforeach
          <div class="invalid-feedback">
            @error('method') {{$message}} @enderror
          </div>
        </div>
      </div>
      <div class="buttons">
        <button class="submitBtn" type="submit">Create game</button>
      </div>
      
    </form>
  </div>
</main>
<style>
      input ~ label{
        background-color: white;
      }
      input:checked ~ label {
        background-color: rgb(130, 215, 255);
        font-style: oblique;
        text-decoration: underline black 1px;
      }
      input:focus ~ label {
        border: 0.1rem solid black;
      }
      .visually-hidden {
        clip: rect(0 0 0 0);
        clip-path: inset(50%);
        height: 1px;
        overflow: hidden;
        position: absolute;
        white-space: nowrap;
        width: 1px;
      }

      main a {
        margin-left: 2%;
      }
      h2 {
        font-family: Arial, Helvetica, sans-serif;
        color:#09639b;
        text-align: center;
        font-size: 1.2rem;
        font-weight: 600;
        
        margin: auto;
      }
      input {
        margin-bottom: 0.5rem;
      }
      

      .supervisors-table{
        border: 0.1rem solid #09639b;
        border-radius: 0.35rem;
      }
      .supervisors-table h3{
        font-family: Arial, Helvetica, sans-serif;
        border-radius: 0.2rem 0.2rem 0 0;
        background-color: #09639b;
        color:white;

        text-align: center;
        font-size: 1rem;
        padding: 0.2rem;
        margin: 0;
      }
      .supervisors-names{
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        flex-wrap: wrap;
      }
      .supervisors-names label{
        width: 10rem;
        text-align: center;
        padding: 0.5rem;
        cursor: pointer;

        /* disable text selection */
        user-select: none; /* supported by Chrome and Opera */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
      }

      .buttons {
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
      }
      .submitBtn {
        background-color: #e73e1c;
        color: white;
        border-radius: 0.2rem;
        border: none;
        padding: 0.5rem;
        margin: 0.5rem;
      }
      .submitBtn:hover {
        background-color: #f87e65;
        color: white;
      }
      
    </style>
@endsection
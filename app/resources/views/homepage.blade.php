@extends('main')

@section('content')
<main class="container">
    <form class="refineItems" novalidate="" method="get">
      <div class="searchbar">
        <input type="text" id="search" name="search" value="{{$request->search}}">
        <button type="submit">Search</button>
      </div>
      <div class="sorting">
        <label for="sortBy" class="form-label">Sort by:</label>
        <select class="form-select @if ($errors->isNotEmpty()) @error('category_id') is-invalid @else is-valid @enderror @endif" id="sortBy" required="" name="sortBy" onchange="this.form.submit()">
          <option value="">Choose...</option>
          <option value="name" @if ($request->sortBy == "name") selected @endif>Name</option>
          <option value="endDate" @if ($request->sortBy == "endDate") selected @endif>Enddate</option>
          <option value="players" @if ($request->sortBy == "players") selected @endif>Players</option>
          <option value="gameState" @if ($request->sortBy == "gameState") selected @endif>Game state</option>
          <option value="killMethod" @if ($request->sortBy == "killMethod") selected @endif>Kill method</option>
        </select>
      </div>
      <a href="{{url('games/add')}}">Add new game</a>
    </form>
  <style>
    .refineItems{
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      width: 100%;
      padding: 0.5rem 0;
      flex-wrap: wrap;
    }
    .searchbar {
      display: flex;
      flex-direction: row;
    }
    .searchbar button{
      color: white;
      background-color: #09639b;
      border: none;
      border-radius: 0 0.2rem 0.2rem 0;
    }
    .searchbar button:hover{
      color: white;
      background-color:#3084b8;
    }

    .sorting {
      display: flex;
      flex-direction: row;
      font-size: 1rem;
      height: 2rem;
      align-items: center;
    }
    .sorting select {
      padding: 0.2rem;
    }
    .sorting label {
      width: 6rem;
      margin-bottom: 0;
    }

    .refineItems a {
      font-size: 1rem;
      height: 1.5rem;
      text-align: center;
      width: 8rem;
      color: white;
      background-color: #3BB497;
      border-radius: 0.5rem;
    }

    .refineItems a:hover {
      color: white;
      background-color:#75d4be;
    }
  </style>

  <div class="row mb-2">
    @foreach ($games as $game)
      <div class="col-md-6">
        <div class="row g-0 border rounded overflow-hidden flex-md-row mb-3 shadow-sm h-md-250 position-relative setHeight">
          <div class="col p-1 d-flex flex-column position-static game-box">
            <h3>{{$game->name}}</h3>
            <div class="row">
              <div class="column">
                <p>Game status: {{ $game->state }}</p>
                <p>Kill method: {{ $game->method->name }}</p>   
              </div>
              <div class="column">
                <p>Players left: {{ $game->playersAlive }}</p>
                <p>Enddate: {{ $game->endDate ? (new \Carbon\Carbon($game->endDate))->format('j/m/y') : "None" }}</p>         
              </div>
            </div>
            <a href="{{ url("games/$game->id")}}" class="button">Details</a>
          </div>
        </div>
      </div>
    @endforeach
  </div>
  <style>
    h3 {
      font-family: var(--bs-font-sans-serif);
      font-weight: 600;
      font-size: 1rem;
    }
    .game-box .column {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      width: fit-content;
      line-height: 1rem;
      padding: 0.5rem;
    }
    .game-box p{
      width: fit-content;
    }
    .game-box .row {
      display: flex;
      flex-direction: row;
      justify-content: space-evenly;
    }
    .button {
      color: white;
      background-color: #09639b;
      border-radius: 0 0 0.2rem 0.2rem;
      width: calc(100% + 0.5rem);
      margin: -0.25rem;
      text-align: center;
    }
    .button:hover {
      color: white;
      background-color: #3084b8;
    }
    .game-box {
      border: #09639b solid 0.1rem;
      border-radius: 0.35rem;
      
    }
    .setHeight {
      height: fit-content;
    }
  </style>
</main>
@endsection
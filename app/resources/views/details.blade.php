@extends('main')

@section('content')
<main>
  <a href="{{ url('/') }}">&lt; back</a>
  <div class="container">
    
    <h2>{{$game->name}}</h2>
    <div class="gamedata">
      <div class="gamedata-item">
        <p>Game status:</p>
        <p>{{$game->state}}</p>
      </div>
      <div class="gamedata-item">
        <p>Kill method:</p>
        <p>{{$game->method->name}}</p>
      </div>
      <div class="gamedata-item">
        <p>End date:</p>
        <p>{{ $game->endDate ? (new \Carbon\Carbon($game->endDate))->format('j/m/y') : "None"}}</p>
      </div>
      <div class="gamedata-item">
        <p>Players alive:</p>
        <p>{{$game->playersAlive}}</p>
      </div>
    </div>

    <div class="buttons">
      <form novalidate="" method="post" action="{{ url("games/$game->id/remove") }}" enctype="multipart/form-data">
        @csrf
        <button class="submitBtn bigActionBtn" type="submit">Delete game</button>
      </form>

      @if ($game->state === 'Created')
        <form novalidate="" method="post" action="{{ url("games/$game->id/start") }}" enctype="multipart/form-data">
          @csrf
          <button class="submitBtn" type="submit">Start game</button>
        </form>
      @elseif ($game->state === 'Active')
        <form novalidate="" method="post" action="{{ url("games/$game->id/stop") }}" enctype="multipart/form-data">
          @csrf
          <button class="submitBtn" type="submit">Stop game</button>
        </form>
      @endif
      {{-- <form novalidate="" method="post" action="{{ url("games/$game->id/archive") }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="messageId" value="1">
        <button class="submitBtn" type="submit">Test kill</button>
      </form> --}}

      <a class="submitBtn" href="{{ url("games/$game->id/dashboard") }}">View dashboard</a>
    </div>

    <hr>
    <form class="needs-validation addPlayer" novalidate="" method="post" action="{{ url("games/$game->id/players/add") }}" enctype="multipart/form-data">
      @csrf
      <input type="text" list="players" oninput="document.getElementById('player').value = this.value;"/>
      <datalist id="players">
        @foreach($potentialPlayers as $player)
          <option data-value="{{ $player->id }}">{{ $player->name }}</option>
        @endforeach
      </datalist>
      <input type="hidden" name="player" value="" id="player">
      <script>
        // toon de value van de option niet bij de dropdown
        const des = Object.getOwnPropertyDescriptor(HTMLInputElement.prototype, 'value');
        Object.defineProperty(HTMLInputElement.prototype, 'value', {
          get: function() {
            const value = des.get.call(this);
          
            if (this.type === 'text' && this.list) {
              const opt = [].find.call(this.list.options, o => o.value === value);
              return opt ? opt.dataset.value : value;
            }
            document.getElementById('player').getAttribute('value') = value;
            return value;  
          } 
        });
      </script>
      <button class="submitBtn" type="submit">Add player</button>
    </form>
    <form class="needs-validation" novalidate="" method="post" action="{{ url("games/$game->id/players/remove") }}" enctype="multipart/form-data">
      @csrf
      <div class="players-table">
        <h3>Select players</h3>
        <div class="players-names">
          @foreach($game->players as $player)
          <div>
            <input class="visually-hidden" type="checkbox" value="{{$player->id}}" id="player{{$player->id}}" name="players[]"><label for="player{{$player->id}}">{{$player->username}}</label>
          </div>
          @endforeach
          <div class="invalid-feedback">
            @error('method') {{$message}} @enderror
          </div>
        </div>
      </div>
      <div class="buttons">
        <button class="submitBtn" type="submit">Remove player</button>
      </div>
    </form>
    
    <style>
      input ~ label{
        background-color: white;
      }
      input:checked ~ label {
        background-color: rgb(130, 215, 255);
        font-style: oblique;
        text-decoration: underline black 1px;
      }
      input:focus ~ label {
        border: 0.1rem solid black;
      }
      .visually-hidden {
        clip: rect(0 0 0 0);
        clip-path: inset(50%);
        height: 1px;
        overflow: hidden;
        position: absolute;
        white-space: nowrap;
        width: 1px;
      }

      main a {
        margin-left: 2%;
      }
      h2 {
        font-family: Arial, Helvetica, sans-serif;
        color:#09639b;
        text-align: center;
        font-size: 1.2rem;
        font-weight: 600;
        
        margin: auto;
      }  
      
      .addPlayer{
        display: flex;
        flex-direction: row;
        justify-content: center;
        flex-wrap:wrap;
        align-items: center;
      }
      .addPlayer input{
        height: 2rem;
      }

      .players-table{
        border: 0.1rem solid #09639b;
        border-radius: 0.35rem;
      }
      .players-table h3{
        font-family: Arial, Helvetica, sans-serif;
        border-radius: 0.2rem 0.2rem 0 0;
        background-color: #09639b;
        color:white;

        text-align: center;
        font-size: 1rem;
        padding: 0.2rem;
        margin: 0;
      }
      .players-names{
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        flex-wrap: wrap;
      }
      .players-names label{
        width: 10rem;
        text-align: center;
        padding: 0.5rem;
        cursor: pointer;

        /* disable text selection */
        user-select: none; /* supported by Chrome and Opera */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
      }


      .buttons{
        display: flex;
        flex-direction: row;
        justify-content: space-evenly;
        flex-wrap: wrap;
        margin: 0 15%;
        text-align: center;
      }
      .buttons button, a{
        min-width: 8rem;
      }
      .submitBtn {
        background-color:#09639b;
        color: white;
        border-radius: 0.2rem;
        border: none;
        padding: 0.5rem;
        margin: 0.5rem;
      }
      .submitBtn:hover {
        background-color:#3084b8;
        color: white;
      }
      .bigActionBtn {
        background-color:#E73E1C;
      }
      .bigActionBtn:hover {
        background-color: #f87e65;
      }
      .buttons a{
        background-color:#3BB497;
      }
      .buttons a:hover{
        background-color:#75d4be;
      }

      .gamedata{
        display:flex;
        flex-direction: column;
        max-width: 15rem;
        margin: auto;
        margin-top: 0.5rem;
      }
      .gamedata .gamedata-item{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
      }      
    </style>
  </div>
</main>
@endsection
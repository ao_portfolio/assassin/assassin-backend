@extends('main')

@section('content')
    
    <main class="container">
        <div class="row g-5">
          <div class="p-4 mb-3 bg-light rounded col-8">
            <h4 class="mb-3">Sign in</h4>
            <form class="needs-validation" novalidate="" method="post" action="{{ url('login') }}">
                @csrf
                <div class="row g-3">

                    <div class="col-12">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="" name="email" value="{{ old('email', '') }}">
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-12">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="" name="password" value="">
                        @error('password')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit">Submit</button>
                </div>
            </form>

            </div>
        </div>
    </main>

@endsection

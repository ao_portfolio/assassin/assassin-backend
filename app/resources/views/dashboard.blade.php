@extends('main')
@section('content')
<main>
  <a href="{{ url("/games/{$game->id}") }}">&lt; back</a>
  <div class="container">
    
    <h2>{{$game->name}}</h2>
    
    <div class="content">
        <div class="players">
            <h3>Players</h3>
            @foreach ($players as $player)
                <div class="player-box">
                    <h4>{{$player->player->username}}</h4>
                    <p>Status: {{ $player->isDead ? 'Dead' : 'Alive' }}</p>
                    <p>Target: {{ $player->target->username ?? 'Unavailable'}}</p>   
                </div>
            @endforeach
        </div>
        <div class="ranking">
            <h3>Ranking</h3>
            <div class="rank-box">
                @foreach ($players->sortBy([
                    ['rank', 'asc'],
                    ['killCount', 'desc'],
                ]) as $player)
                    <p>{{($player->rank ?? '?') . "\t" . $player->player->username}}</p>
                @endforeach
            </div>
        </div>
    </div>
</div>

<style>
    main a {
        margin-left: 2%;
    }
    h2 {
        font-family: Arial, Helvetica, sans-serif;
        color:#09639b;
        text-align: center;
        font-size: 1.2rem;
        font-weight: 600;
        
        margin: auto;
    }  
    h3 {
        font-family: Arial, Helvetica, sans-serif;
        color:#09639b;
        text-align: left;
        font-size: 1rem;
        font-weight: 600;
        width: 100%;
    }
    p {
        margin: 0;
        font-size: 0.8rem;
    }

    .content {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        flex-wrap: wrap;
    }
    .players {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: space-evenly;
        max-width: 30rem;
    }
    .players h4 {
        font-family: Arial, Helvetica, sans-serif;
        text-align: left;
        font-size: 0.9rem;
        font-weight: 600;
        margin:0;
    }
    .player-box {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        overflow-wrap: break-word;
        justify-content: space-evenly;
        padding: 0.2rem;
        margin: 0.2rem;
        border: 0.1rem solid #09639b;
        border-radius: 0.3rem;
        min-width: 8rem;
    }
    .rank-box {
        padding: 0.3rem;
        border: 0.1rem solid #09639b;
        border-radius: 0.3rem;
    }

</style>

</main>
@endsection